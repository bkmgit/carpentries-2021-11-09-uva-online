1+100
1+
200  
3 + 5 * 2
(3 + 5) * 2
(3 + ( 5 * ( 2 ^ 2))) # hard to read
3 + 5 * 2 ^ 2 # clear if you remember the rules
3 + 5 * (2 ^ 2) # easier if you forgot some rules
2/100000 # division
5e4 # Note no minus sign
# Mathematical functions
sin(1) # a trigonometic function
log(1) # a natural logarithm
log10(100) # base-10 logarithm
exp(0.5) # e^(1/2)
# Comparing things
0.5 == 1/2 # is equal to?
exp(0.5) == exp(1/2)
e^(1/2)
exp(1)
#use a variable
e = exp(1)
e^(1/2) == exp(0.5)
# continue comparisons
1 != 2 # check inequality
1 !=1 
1 < 2 # less than
1 <= 1 # less than or equal to
1 >= -9.5 # greater than or equal to 
# floating point numbers
1 + 1e-100 == 1
?all.equal
all.equal(1+1e-100,1)
?all.equal
?identical
identical(1+1e-100,1)
identical(1+1e-100,1, num.eq = FALSE)
identical(1+1e-10,1)
identical(1+1e-16,1)
identical(1+1e-16,1,num.eq=FALSE)
all.equal(1+1e-16,1)
all.equal(1+1e-10,1,tolerance=1e-6)
all.equal(1+1e-10,1,tolerance=1e-11)
# Floating point is tricky!
# http://floating-point-gui.de

# Variables

x <- 1/40
log(x)
x <- 100
x <- x+1 # check value in environment
y <= x*2 # typo
y <- x*2
## Exercise
# Which of the following are valid
# R variable names
# min_height
# max.height
# _age
# .mass
# MaxLength
# min-length
# 2widths
# celsius2kelvin

min_height <- 2
max.height <- 4
_age <- 7
.mass <- 8
MaxLength <- 9
min-length <- 2
2widths <- 8
celsius2kelvin <- 88
min height <- 4

# Vectorization

1:5
2^(1:5)
x <- 1:5
2^x
# Environment Management
ls()
ls(all.names=TRUE)
ls
ls()
rm(x) # delete variable from environment
# Installing packages
install.packages("ggplot2")
install.packages("plyr")
install.packages("gapminder")
# Check installed packages
installed.packages()
## Summary
# <- assign values to variables/objects
# ls() list variables/objects
# rm() delete variables/objects
# install.packages() install packages/libraries
# Use R as a calculator

# Data Structures

dir.create("data") # create folder
cats <- data.frame(coat = c("calico",
                            "black",
                            "tabby"),
                   weight = c(2.1,
                              5.0,
                              3.2),
                   likes_string = c(1,
                                    0,
                                    1))
View(cats)
cat_weight = c(2.1,5.0,3.2) # create vector
cat_coat = c("calico","black","tabby")
cat_likes_string = c(1,0,1)
# put vectors in a data frame
cats_again <- data.frame(cat_weight,
                   cat_coat,
                   cat_likes_string)
# View data frame
View(cats_again)
# Combine example
1:4
c(1,2,3,4)
cat_coat
cat_weight
# Better is
cat_coat <- c("calico",
              "black",
              "tabby")
# Missing values or incorrect assignment
cats$weight + cats$coat
bad_output <- cats$weight + cats$coat
bad_output
example_missing <- c(1,NA,3)
example_missing
cats$weight + example_missing
data.frame

row.names(cats) # Get row names
row.names(cats_again) <- c("Dijkstra",
                           "Felix",
                           "Tom")
View(cats_again)
cats_again$eats_mice <- c(1,1,0)
View(cats_again)
# Save data to text file
write.csv(x=cats,
          file="data/feline-data.csv",
          row.names=FALSE)
# Read in data
copy_cats <- read.csv(file="data/feline-data.csv",
                      stringsAsFactors=TRUE)
View(copy_cats)
# operate on a column
paste("My cat is",copy_cats$coat)

# Data types
typeof(copy_cats$weight)
# numeric types include
## double, integer,  complex
# other types are
## character, raw, list, logical

# examine summary of data frame
str(copy_cats)
# examine different types
typeof(3.14)
typeof(3L)
typeof(1+1i)
typeof(TRUE)
typeof('banana')

# this is not reasonable
1 + 'banana'
1 + 1.15
1L + 1.15
'Tom ' + 'Jerry'
paste('Tom ','Jerry')
Tom_and_Jerry = paste('Tom ','Jerry')
paste('Tom','Jerry') # space inserted
paste0('Tom','Jerry') # no space inserted

# Vectors and Type Coercion

my_vector <- vector(length=3)
my_vector
# created a logical vector

another_vector <- vector(mode='character',
                         length=3)
another_vector
str(another_vector)
vector_another <- vector(length=3,
                         mode='character')
str(vector_another)
vector_another == another_vector
# exercise
quiz_vector <- c(2,6,'3')
str(quiz_vector)
# forced conversion
character_coerced_to_numeric <- 
  as.numeric(quiz_vector)
str(character_coerced_to_numeric)
numeric_coerced_logical <-
  as.logical(character_coerced_to_numeric)
numeric_coerced_logical
test_coercion_logical <- c(0,1,2,0)
as.logical(test_coercion_logical)
as.logical(c(-1,0,1,0))
class(test_coercion_logical)
class(as.logical(c(-1,0,1,0)))
# Further numeric example
as.numeric(c("-.1"," 2.7 ", "B"))
